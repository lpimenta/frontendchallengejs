
var renderer = (function () {
	var dataContainer = window.body;

	function validItemToRender(item) {
		return item.text !== undefined && item.type !== undefined && item.by !== undefined;
	}

	function renderItemsLine(item) {
		if (!validItemToRender(item)) {
			return;
		}
		var trElement = document.createElement('tr'),
			tdElementText = document.createElement('td'),
			tdElementType = document.createElement('td'),
			tdElementBy = document.createElement('td'),
			tdElementId = document.createElement('td'),
			author = document.createTextNode(item.by),
			text = document.createTextNode(item.text),
			type = document.createTextNode(item.type),
			by = document.createTextNode(item.by),
			id = document.createTextNode(item.id);

		tdElementText.innerHTML = item.text;
		tdElementText.className = "newshacker-text";
		tdElementType.appendChild(type);
		tdElementType.className = "newshacker-type";
		tdElementBy.appendChild(by);
		tdElementBy.className = "newshacker-author";
		tdElementId.appendChild(id);
		trElement.appendChild(tdElementId);
		trElement.appendChild(tdElementBy);
		trElement.appendChild(tdElementText);
		trElement.appendChild(tdElementType);
		setTimeout(function () {
            dataContainer.appendChild(trElement);
        }, 0);
	}

	function setDataContainer(value) {
		dataContainer = value;
	}

	return {
		setDataContainer: setDataContainer,
		renderItemsLine : renderItemsLine
	};
})();



var hackerNewsItems = (function () {
	var requests = {
		"topStories" : "https://hacker-news.firebaseio.com/v0/topstories.json?print=pretty",
		"getItem" : function (id) { return "https://hacker-news.firebaseio.com/v0/item/" + id + ".json?print=pretty"},
		"getMaxItemId" : "https://hacker-news.firebaseio.com/v0/maxitem.json?print=pretty"
	},
        maxItemsNumber,
        lastItem,
        requestsPerScroll = 80,
        itemsToRender = [],
        requesting = false,
        renderItemLine,
        multiple = false,
        xhttp;


	function getItems() {
		var fromItem = lastItem - 1,
			toItem = (fromItem - requestsPerScroll) > 1 ? (fromItem - requestsPerScroll) : 1,
            i;
	    
	    if (toItem == 1) {
            return;
	    }

	    requesting = true;
	    if (!multiple) {
			getItem(fromItem, toItem);
		} else {
			for (i = fromItem - 1; i >= toItem; i--) {
				getItem(i, toItem);
			}
		}
	}
    
    function initiator(renderItemCallBack) {
		renderItemLine = renderItemCallBack;
		xhttp = new XMLHttpRequest();
		xhttp.onreadystatechange = function () {
			if (xhttp.readyState == 4) {
				requesting = false;
			}
			//console.log(requesting);
			if (xhttp.readyState == 4 && xhttp.status == 200) {
				maxItemsNumber = Number(xhttp.responseText);
                lastItem = maxItemsNumber;
                getItems();
                xhttp = undefined;
			}
		};
		requesting = true;
		xhttp.open("GET", requests.getMaxItemId, true);
		xhttp.send();
	}

	function processRequestCallBack(id, toItem, xhttp) {
		if (!multiple) {
			return function () {
                if (xhttp.readyState == 4) {
                    requesting = (id == toItem) ? false : requesting;

                    //console.log(id, toItem);
                    if (id >= toItem) {
                        getItem(id - 1, toItem);
                    }

                }
                //console.log(requesting);

                if (xhttp.readyState == 4 && xhttp.status == 200) {
                    var obj = JSON.parse(xhttp.responseText);
                    if (obj) {
                        renderItemLine(obj);
                    }

                    lastItem = id;
                    xhttp = null;
                }
            };
		} else {
			return function () {
                if (xhttp.readyState == 4) {
                    //console.log(id, toItem);
                    requesting = (id == toItem) ? false : requesting;
                }
                //console.log(requesting);

                if (xhttp.readyState == 4 && xhttp.status == 200) {
                    var obj = JSON.parse(xhttp.responseText);
                    if (obj) {
                        renderItemLine(obj);
                    }

                    lastItem = id;
                    xhttp = null;
                }
            };
		}
	}

	function getItem(id, toItem) {
		var xhttp = new XMLHttpRequest(), obj;

		xhttp.onreadystatechange = processRequestCallBack(id, toItem, xhttp);
		xhttp.open("GET", requests.getItem(id), true);
		xhttp.send();
	}

	function isRequesting() {
		return requesting;
	}

	function setMultiple(value) {
		multiple = value;
	}

	return {
		initiator: initiator,
		getItems: getItems,
		isRequesting: isRequesting,
		setMultiple: setMultiple
	};

})();

renderer.setDataContainer(document.getElementsByClassName('data-container')[0]);
// Multiple makes pararlell calls, non multiple makes sequencial and one at a time
hackerNewsItems.setMultiple(false);
hackerNewsItems.initiator(renderer.renderItemsLine);

window.addEventListener("scroll", function () {
	if (!hackerNewsItems.isRequesting() && (window.innerHeight + window.scrollY) >= document.body.offsetHeight) {
		//console.log('Hit it!!');
        hackerNewsItems.getItems();
    }
});


